# Projet Pacman, partie web

### Installation du projet

* Créer une base de donnéees et importer le fichier bdd_pacman.sql
* Modifier les champs 'nomutilisateur' et 'motdepasse' du fichier src/dao.properties avec vos identifiants de connexion à la base de donnéees

### Lancement du projet

* Pour lancer le projet, il faut lancer la servlet PacmanServlet ou aller à l'addresse /PacmanServlet