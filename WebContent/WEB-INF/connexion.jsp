<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<!-- CDN Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
	<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"> </script>
	
	<link type="text/css" rel="stylesheet" href="css/connexion.css"/>
	
	<title>Connexion au jeu Pacman</title>
</head>
<body>
<body>
<div class="container">
	<div class="row">
		<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
			<div class="card card-signin my-5">
				<div class="card-body">
					<h5 class="card-title text-center">Connexion</h5>
					<form class="form-signin" method="post" action="ConnexionServlet">
						<div class="form-label-group input-text">
							<label>Identifiant</label>
							<input id="inputEmail" name="login" class="form-control" placeholder="Votre identifiant" required autofocus>
						</div>
	
						<div class="form-label-group input-text">
							<label for="inputPassword">Mot de passe</label>
							<input type="password" id="inputPassword" name="mdp" class="form-control" placeholder="Password" required>
						</div>
	
						<button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">CONNEXION</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="text-align:center;">
	<a href="/pacman/InscriptionServlet">Inscription d'un nouvel utilisateur</a>
</div>

<c:if test = "${ !empty form_connexion.resultat}">
	<div style="text-align: center; color: red;">
		<c:out value="${ form_connexion.resultat }"/>
	</div>
</c:if>
</body>
</html>