<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<!-- CDN Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
	<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"> </script>
	
	<link type="text/css" rel="stylesheet" href="css/connexion.css"/>
<title>Pacman - Parties</title>
</head>
<body>
	<div style="text-align: center; margin-top: 15px;">
		<h1>LISTE DE VOS PARTIES</h1>
	</div>
	
	<br />
	
	<c:if test="${ empty list_game }">
		<div>
			Vous n'avez pas encore joué une partie de Pacman...
		</div>	
	</c:if>
	
	<!-- ELSE -->
	<c:if test="${ !empty list_game }">
		<table class="table">
			<thead>
			    <tr>
			      <th scope="col">Id_game</th>
			      <th scope="col">Utilisateur</th>
			      <th scope="col">Date</th>
			      <th scope="col">Score</th>
			      <th scope="col">Statut</th>
			    </tr>
			  </thead>
			  <tbody>
				<c:forEach var="game" items="${ list_game }">
				    <tr>
				      <th scope="row"><c:out value="${ game.id }"/></th>
				      <td><c:out value="${ user_name }"/></td>
				      <td><c:out value="${ game.date }"/></td>
				      <td><c:out value="${ game.score }"/></td>
				      <td>
				      	<c:if test="${ game.gagne }">
				      		<c:out value="Victoire" />
				      	</c:if>
				      	<c:if test="${ !game.gagne }">
				      		<c:out value="Perdu" />
				      	</c:if>
				      </td>
				    </tr>
				</c:forEach>
			</tbody>
		</table>	
	</c:if>
</body>
</html>