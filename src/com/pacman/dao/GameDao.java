package com.pacman.dao;

import java.util.List;
import com.pacman.beans.Game;

public interface GameDao {
	
	void createGame(Game game) throws DAOException;
	Game getGame(Long id) throws DAOException;
	List<Game> findByUser(Long id_user) throws DAOException;

}
