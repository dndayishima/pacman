package com.pacman.dao;

import java.util.List;
import com.pacman.beans.User;

public interface UserDao {
	
	void createUser(User user) throws DAOException;
	User getUser(String login, String password) throws DAOException;
	User findUserById(Long id) throws DAOException;
	List<User> findAll() throws DAOException;

}