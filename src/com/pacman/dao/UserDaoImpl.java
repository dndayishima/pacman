package com.pacman.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.pacman.beans.User;

public class UserDaoImpl implements UserDao {

	private DAOFactory daoFactory;

	public UserDaoImpl(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	@Override
	public void createUser(User user) throws DAOException {
		Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    
	    try {
	    	connexion = daoFactory.getConnection();
	    	String requete = "INSERT INTO User (login, password) VALUES (?, ?)";
	    	preparedStatement = DaoUtils.initRequete(connexion, requete, true, user.getLogin(), user.getMdp());
	    	int status = preparedStatement.executeUpdate();
	    	
	    	if (status == 0) {
	    		throw new DAOException("Echec de la création de l'utilisateur");
	    	}
	    	
	    	//récupération de l'id
	    	resultSet = preparedStatement.getGeneratedKeys();
	    	if (resultSet.next()) {
	    		user.setId(resultSet.getLong(1));
	    	} else {
	    		throw new DAOException("Echec de la création de l'utilisateur");
	    	}
	    } catch(SQLException e) {
	    	throw new DAOException(e);
	    } finally {
	    	DaoUtils.fermetureRessources(resultSet, preparedStatement, connexion);
	    }
	}

	@Override
	public User getUser(String login, String password) throws DAOException {
		Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    User user = null;
	    
	    try {
	    	connexion = daoFactory.getConnection();
	    	String requete = "SELECT id, login, password, score FROM User WHERE login = ? AND password = ?";
	    	preparedStatement = DaoUtils.initRequete(connexion, requete, false, login, password);
	    	resultSet = preparedStatement.executeQuery();
	    	
	    	if (resultSet.next()) {
	    		user = map(resultSet);
	    	}
	    } catch (SQLException e) {
	    	throw new DAOException(e);
	    } finally {
	    	DaoUtils.fermetureRessources(resultSet, preparedStatement, connexion);
	    }
	    
	    return user;
	}
	
	@Override
	public User findUserById(Long id) throws DAOException {
		Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    User user = null;
	    
	    try {
	    	connexion = daoFactory.getConnection();
	    	String requete = "SELECT id, login, password, score FROM User WHERE id = ?";
	    	preparedStatement = DaoUtils.initRequete(connexion, requete, false, id);
	    	resultSet = preparedStatement.executeQuery();
	    	
	    	if (resultSet.next()) {
	    		user = map(resultSet);
	    	}
	    } catch (SQLException e) {
	    	throw new DAOException(e);
	    } finally {
	    	DaoUtils.fermetureRessources(resultSet, preparedStatement, connexion);
	    }
	    
	    return user;
	}
	
	@Override
	public List<User> findAll() throws DAOException {
		Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    List<User> liste = new ArrayList<User>();
	    
	    try {
	    	connexion = daoFactory.getConnection();
	    	String requete = "SELECT id, login, password, score FROM User";
	    	preparedStatement = DaoUtils.initRequete(connexion, requete, false);
	    	resultSet = preparedStatement.executeQuery();
	    	
	    	while (resultSet.next()) {
	    		User user = map(resultSet);
	    		liste.add(user);
	    	}
	    } catch (SQLException e) {
	    	throw new DAOException(e);
	    } finally {
	    	DaoUtils.fermetureRessources(resultSet, preparedStatement, connexion);
	    }
	    
	    return liste;
	}

	/*
	 * Méthode permettant de faire le mapping entre un ResultSet et un bean
	 * Utilisateur.
	 */
	private static User map(ResultSet resultSet) throws SQLException {
		User user = new User();
		user.setId(resultSet.getLong("id"));
		user.setMdp(resultSet.getString("password"));
		user.setLogin(resultSet.getString("login"));
		user.setScore(resultSet.getInt("score"));
		return user;
	}

	/*@Override
	public void createUser(User user) throws DAOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User getUser(String login) throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}*/

}

