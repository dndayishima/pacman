package com.pacman.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pacman.beans.Game;

public class GameDaoImpl implements GameDao {
	
	private DAOFactory daoFactory;

	GameDaoImpl(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	@Override
	public void createGame(Game game) throws DAOException {
		Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    
	    try {
	    	connexion = daoFactory.getConnection();
	    	String requete = "INSERT INTO Game (user_id, date, score, gagne) VALUES (?, ?, ?, ?)";
	    	preparedStatement = DaoUtils.initRequete(connexion, requete, true, game.getId_user(), game.getDate(), game.getScore(), game.isGagne());
	    	int status = preparedStatement.executeUpdate();
	    	
	    	if (status == 0) {
	    		throw new DAOException("Echec de la création de la partie");
	    	}
	    	
	    	//récupération de l'id
	    	resultSet = preparedStatement.getGeneratedKeys();
	    	if (resultSet.next()) {
	    		game.setId(resultSet.getLong(1));
	    	} else {
	    		throw new DAOException("Echec de la création de la partie");
	    	}
	    } catch(SQLException e) {
	    	throw new DAOException(e);
	    } finally {
	    	DaoUtils.fermetureRessources(resultSet, preparedStatement, connexion);
	    }
	}

	@Override
	public Game getGame(Long id) throws DAOException {
		Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    Game game = null;
	    
	    try {
	    	connexion = daoFactory.getConnection();
	    	String requete = "SELECT id, id_user, date, score, gagne FROM Game WHERE id = ?";
	    	preparedStatement = DaoUtils.initRequete(connexion, requete, false, id);
	    	resultSet = preparedStatement.executeQuery();
	    	
	    	if (resultSet.next()) {
	    		game = map(resultSet);
	    	}
	    } catch (SQLException e) {
	    	throw new DAOException(e);
	    } finally {
	    	DaoUtils.fermetureRessources(resultSet, preparedStatement, connexion);
	    }
	    
	    return game;
	}

	@Override
	public List<Game> findByUser(Long id_user) throws DAOException {
		Connection connexion = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    List<Game> liste = new ArrayList<Game>();
	    
	    try {
	    	connexion = daoFactory.getConnection();
	    	String requete = "SELECT id, id_user, date, score, gagne FROM Game WHERE id_user = ?";
	    	preparedStatement = DaoUtils.initRequete(connexion, requete, false, id_user);
	    	resultSet = preparedStatement.executeQuery();
	    	
	    	while (resultSet.next()) {
	    		Game game = map(resultSet);
	    		liste.add(game);
	    	}
	    } catch (SQLException e) {
	    	throw new DAOException(e);
	    } finally {
	    	DaoUtils.fermetureRessources(resultSet, preparedStatement, connexion);
	    }
	    
	    return liste;
	}
	
	/*
	 * Méthode permettant de faire le mapping entre un ResultSet et un bean
	 * Game.
	 */
	private static Game map(ResultSet resultSet) throws SQLException {
		Game game = new Game();
		game.setId(resultSet.getLong("id"));
		game.setId_user(resultSet.getLong("id_user"));
		game.setDate(resultSet.getDate("date"));
		game.setScore(resultSet.getInt("score"));
		game.setGagne(resultSet.getBoolean("gagne"));
		return game;
	}

}

