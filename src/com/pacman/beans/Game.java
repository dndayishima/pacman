package com.pacman.beans;

import java.sql.Date;

public class Game {

	private Long id;
	private Long id_user;
	private Date date;
	private int score;
	private boolean gagne;

	public Long getId() {
		return id;
	}

	public Long getId_user() {
		return id_user;
	}

	public Date getDate() {
		return date;
	}

	public int getScore() {
		return score;
	}

	public boolean isGagne() {
		return gagne;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setId_user(Long id_user) {
		this.id_user = id_user;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void setGagne(boolean gagne) {
		this.gagne = gagne;
	}

}

