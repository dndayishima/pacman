package com.pacman.beans;

public class User {

	private Long id;
	private String login;
	private String mdp;
	private int score;

	public Long getId() {
		return id;
	}

	public String getLogin() {
		return login;
	}

	public String getMdp() {
		return mdp;
	}

	public int getScore() {
		return score;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public String toString() {
		return "Utilisateur - id : " + this.id + " - login : " + this.login; 
	}

}

