package com.pacman.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pacman.beans.Game;
import com.pacman.beans.User;
import com.pacman.dao.DAOFactory;
import com.pacman.dao.GameDao;
import com.pacman.dao.UserDao;

/**
 * Servlet implementation class PartiesServlet
 */
@WebServlet("/PartiesServlet")
public class PartiesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String CONF_DAO_FACTORY = "daofactory";
	private UserDao userDao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PartiesServlet() {
        super();
    }
    
    public void init() throws ServletException {
    	this.userDao = ((DAOFactory)getServletContext().getAttribute(CONF_DAO_FACTORY)).getUtilisateurDao();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long id_user = (Long) request.getAttribute("id_user");
		GameDao gameDao = ((DAOFactory)getServletContext().getAttribute(CONF_DAO_FACTORY)).getGameDao();
		List<Game> list = gameDao.findByUser(id_user);
		String user_name = userDao.findUserById(id_user).getLogin();
		request.setAttribute("list_game", list);
		request.setAttribute("user_name", user_name);
		this.getServletContext().getRequestDispatcher("/WEB-INF/parties.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
