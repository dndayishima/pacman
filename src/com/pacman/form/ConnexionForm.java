package com.pacman.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.pacman.beans.User;

public class ConnexionForm {
	
	private static final String CHAMP_LOGIN = "login";
	private static final String CHAMP_MDP = "mdp";
	
	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();

	public String getResultat() {
		return resultat;
	}

	public Map<String, String> getErreurs() {
		return erreurs;
	}
	
	public User connexion(HttpServletRequest request) {
		String login = getValeurChamp(request, CHAMP_LOGIN);
		String mdp = getValeurChamp(request, CHAMP_MDP);
		
		User user = new User();
		
		try {
			validationLogin(login);
		} catch (Exception e) {
			setErreur(CHAMP_LOGIN, e.getMessage());
		}
		user.setLogin(login);
		
		try {
			validationMdp(mdp);
		} catch (Exception e) {
			setErreur(CHAMP_MDP, e.getMessage());
		}
		user.setMdp(mdp);
		
		if (erreurs.isEmpty()) {
			resultat = "Succès de la connexion";
		} else {
			resultat = "Echec de la connexion";
		}
		
		return user;
	}
	
	private void validationLogin(String login) throws Exception {
		if (login == null) {
			throw new Exception("Merci de saisir un login.");
		}
	}
	
	private void validationMdp(String mdp) throws Exception {
		if (mdp != null) {
			if (mdp.length() < 3) {
				throw new Exception("Le mot de passe doit contenir au moins 3 caractères");
			}
		} else {
			throw new Exception("Merci de saisir votre mot de passe");
		}
	}
	
	/*
	 * Ajoute un message correspondant au champ spécifié à la map des erreurs.
	 */
	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	/*
	 * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
	 * sinon.
	 */
	private static String getValeurChamp(HttpServletRequest request, String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur.trim();
		}
	}

}
